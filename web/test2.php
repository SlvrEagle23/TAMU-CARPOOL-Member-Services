<?php


require_once dirname(__FILE__) . '/../app/bootstrap.php';
$application->bootstrap();

print_r($_REQUEST);
exit;


$request = new Zend_Http_Client('http://carpoolonline.tamu.edu/test2.php');

$request->setParameterPost(array(
    'posting_key'       => 'TEST',
    'EXT_TRANS_ID'      => '785DC001326',
    'sys_tracking_id'   => '334530',
    'card_type'         => 'MAST',
    'name_on_acct'      => 'Sumana Datta',
    'pmt_status'        => 'success',
    'tpg_trans_id'      => '20110831000014'
));

$response = $request->request('POST');

echo $response->getBody();

exit;

$to_convert = Doctrine_Query::create()
    ->from('Rides r')
    ->where('r.ndr_id IS NULL')
    ->execute();

foreach($to_convert as $row)
{
    $date = date('Y-m-d', strtotime($row->ridedate));
    
    // Attempt to find a night associated with the same date as this NDR.
    $ndr = Doctrine_Query::create()
        ->from('Ndr n, n.Event e')
        ->where('e.event_date = ?', $date)
        ->whereIn('e.category', array('night', 'theme'))
        ->fetchOne();
    
    if ($ndr instanceof Ndr)
    {
        $row->ndr_id = $ndr->id;
        $row->save();
    }
}

echo 'Done';


/*
if (!$_COOKIE['msccAuth'])
{
	header("Location: https://secure.mscc.tamu.edu/sso/?target=".urlencode("http://carpoolonline.tamu.edu/test.php"));
	exit;
}
else
{
	$auth_cookie = $_COOKIE['msccAuth'];
	$results_raw = file_get_contents('http://api.msc.tamu.edu/sso/shibboleth.json?cookie='.$auth_cookie.'&auth=3164025d1b07e120782000eb3b573dda');
	$results = Zend_Json::decode($results_raw);
	DF_Utilities::print_r($results);
	exit;
}
*/