<?php
/**
 * Event management page
 */
use \Entity\Event;
use \Entity\User;
use \Entity\Signup;

class Events_EventController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::isAllowed('access member features');
	}
	
	public function preDispatch()
	{
		// Retrieve the basic event information from the database.
		$this->event_num = intval($this->_getParam('num'));
		$event = Event::find($this->event_num);
        
		if (!($event instanceof Event))
			throw new \DF\Exception\DisplayOnly('Event Not Found: The event you specified could not be found in the system.');
		
        $this->event_obj = $event;
		$event = $event->toArray();
		$event['category_text'] = $this->config->carpool->event_types->{$event['category']};
		$this->view->event = $this->event = $event;
		
		// Pull the signup lists for this event.
        $members_signed_up_raw = $this->em->createQuery('SELECT s, u, r FROM Entity\Signup s JOIN s.user u JOIN u.roles r WHERE s.event_id = :event_id ORDER BY u.lastname ASC, u.firstname ASC, u.username ASC')
            ->setParameter('event_id', $event['num'])
            ->getArrayResult();
        
		$members_signed_up = array();
		$sms_recipients = array();
		$email_recipients = array();
		
		foreach($members_signed_up_raw as $member)
		{
			$user_id = $member['user']['id'];
			$member['user']['avatar'] = User::getUserAvatar($member['user']['uin']);
			$member['user']['num'] = $this->event['num'];
			$gender = ($member['user']['sex'] == "M") ? "M" : "F";
			
			if ($member['status'] == CP_SIGNUP_TAKEMYNIGHT)
			{
				$members_signed_up['takemynight'][$user_id] = $member;
			}
			else
			{
				$members_signed_up['all'][$user_id] = $member;
				
				if (stristr($member['user']['roles'][0]['name'], 'Member') !== FALSE)
				{
					$members_signed_up['members'][$user_id] = $member;
					$members_signed_up[$gender][$user_id] = $member;
				}
				else if (stristr($member['user']['roles'][0]['name'], 'Advisor') !== FALSE)
				{
					$members_signed_up['advisors'][$user_id] = $member;
					$members_signed_up['advisors_'.$gender][$user_id] = $member;
				}
				else
				{
					$members_signed_up['execs'][] = $member;
					$members_signed_up['execs_'.$gender][$user_id] = $member;
				}
			}
			
			$sms_recipients[] = $member['user']['id'];
			$email_recipients[] = $member['user']['email'];
		}
		
		$this->view->email_recipients = implode('; ', $email_recipients);
		$this->view->sms_recipients = implode(',', $sms_recipients);
		
		$this->members_signed_up = $this->view->members_signed_up = $members_signed_up;
	}
	
	public function indexAction()
	{
		// Admin signup options
		if (\DF\Acl::isAllowed('access director features'))
		{
			$user_select = array();
            $users = $this->em->createQuery('SELECT u FROM Entity\User u ORDER BY u.lastname ASC')->getArrayResult();
            
			foreach((array)$users as $member)
			{
				if (!in_array($member['id'], (array)$this->members_signed_up['all']))
				{
					$user_select[$member['uin']] = $member['lastname'].', '.$member['firstname'];
				}
			}
			
			$this->view->user_select = $user_select;
		}
			
		// User signup options
		$user = \DF\Auth::getLoggedInUser();
		$can_signup = $this->canSignup($user);
        
        $is_signed_up_obj = Signup::getRepository()->findOneBy(array('event_id' => $this->event_num, 'user_id' => $user->id));
        $is_signed_up_raw = ($is_signed_up_obj instanceof Signup);
		
		$this->view->can_signup = $can_signup;
		$this->view->is_signed_up = ($is_signed_up_raw && $is_signed_up_raw->status != CP_SIGNUP_TAKEMYNIGHT);
	}
	
	/**
	 * (Admin) Remove a user from an event.
	 */
	public function removeAction()
	{
        \DF\Acl::checkPermission('access director features');
        
        $user_id = (int)$this->_getParam('user_id');
        
		if ($user_id != 0)
		{
			// Delete any existing record whether signing up or removing signup.
			$signup = Signup::getRepository()->findOneBy(array('event_id' => $this->event_num, 'user_id' => $user_id));
			$signup->delete();
		}
		
		$this->alert('User removed from event.');
		$this->redirectFromHere(array('action' => 'index', 'user_id' => NULL));
		return;
	}
	
	/**
	 * (Admin) Print member agreements.
	 */
	public function agreementAction()
	{
		$this->acl->checkPermission('access director features');

		$members_by_gender = array(
			'M' => array_merge((array)$this->members_signed_up['M'], (array)$this->members_signed_up['execs_M']),
			'F' => array_merge((array)$this->members_signed_up['F'], (array)$this->members_signed_up['execs_F']),
		);

		$this->view->members_by_gender = $members_by_gender;
	}

	/**
	 * (Admin) Special food needs.
	 */
	public function foodneedsAction()
	{
		$this->acl->checkPermission('access director features');

		$food_needs = array();

		foreach($this->members_signed_up['all'] as $member)
		{
			if ($member['food_needs'])
				$food_needs[] = $member;
		}

		$this->view->food_needs = $food_needs;
	}
	
	/**
	 * Set the user's status to "take my night", a special status for working nights.
	 */
	public function takemynightAction()
	{
		$user = \DF\Auth::getInstance()->getLoggedInUser();
		
		if ($this->event['category'] == "night")
		{
			$signup = Signup::getRepository()->findOneBy(array(
                'event_id' => $this->event_num,
                'user_id' => $user->id
            ));
			$signup->status = CP_SIGNUP_TAKEMYNIGHT;
			$signup->timestamp = time();
			$signup->save();
		}
		
		$this->alert('Signup status updated!');
		$this->redirectFromHere(array('action' => 'index'));
	}
	
	/**
	 * Register/unregister for an event
	 */
	public function toggleAction()
	{
		$current_user = \DF\Auth::getLoggedInUser();
		
		if ($this->_hasParam('uin'))
			$user = User::getOrCreate($this->_getParam('uin'));
		else
			$user = $current_user;
		
		// Permission check/user lookup.
		if (!$user || ($user->uin != $current_user->uin && !\DF\Acl::getInstance()->isAllowed('access director features')))
			throw new \DF\Exception\DisplayOnly('User not found!');
		
		$signup = Signup::getRepository()->findOneBy(array('event_id' => $this->event_num, 'user_id' => $user->id));
		
		$is_signed_up = ($signup instanceof Signup);
		$can_sign_up = $this->canSignup($user);
		
		// Delete any existing record whether signing up or removing signup.
		if ($is_signed_up)
			$signup->delete();
		else if (!$can_sign_up)
			throw new \DF\Exception\DisplayOnly('You cannot sign up for this event at this time.');
		
		if (!$is_signed_up || $signup->status == CP_SIGNUP_TAKEMYNIGHT)
		{
			if (!\DF\Acl::userAllowed('access exec features', $user))
			{
				// Look for someone on this event who is listed as "take my night", and remove their signup if applicable.
				if ($this->event['category'] == "night")
				{
                    try
                    {
                        $takemynight_user = $this->em->createQuery('SELECT s, u FROM Entity\Signup s JOIN s.user u WHERE s.event_id = :event_id AND s.status = :status AND u.sex = :sex ORDER BY s.timestamp ASC')
                            ->setParameters(array('event_id' => $this->event_num, 'status' => CP_SIGNUP_TAKEMYNIGHT, 'sex' => $user->sex))
                            ->getSingleResult();
                        
                        if ($takemynight_user)
                            $takemynight_user->delete();
                    }
                    catch(\Exception $e) {}
				}
			}
            
			$new_signup = new Signup();
			$new_signup->event = $this->event_obj;
			$new_signup->user = $user;
			$new_signup->timestamp = time();
			$new_signup->status = CP_SIGNUP_UNCONFIRMED;
			$new_signup->save();
		}
		
		$this->alert('Signup status toggled for this event.');
		$this->redirectFromHere(array('action' => 'index'));
		return;
	}
	
	/**
	 * Internal check for signup ability.
	 */
	protected function canSignup($user = NULL)
	{
		// $event_timestamp = strtotime($this->event['event_date'].' 12:00 am')+86400;
        $current_user = \DF\Auth::getLoggedInUser();
		
		if (!($user instanceof User))
            $user = $current_user;
        
		if (\DF\Acl::userAllowed('access exec features', $current_user))
		{
			return TRUE;
		}
		else
		{
			if ($this->event['is_open'] == 1)
			{
				// If a user limit is in place, check the current gender vs. the limit.
				if ($this->event['max_ppl'] != 0)
				{
					if ($this->event['category'] == "night")
					{
						$guycount = count($this->members_signed_up['M']);
						$girlcount = count($this->members_signed_up['F']);
						
						if (($user['sex'] == "M" && $guycount >= $this->event['max_ppl']) || ($user['sex'] == "F" && $girlcount >= $this->event['max_ppl']))
							return FALSE;
					}
					else
					{
						$memcount = count($this->members_signed_up['members']);
						if ($memcount >= $this->event['max_ppl'])
							return FALSE;
					}
				}
			}
			else
			{
				return FALSE;
			}
			
			if (empty($user['sex']))
			{
				$this->alert('<b>You have not specified a gender on your profile!</b><br />You are required to complete your profile before signing up for events. To edit your profile, <a href="'.\DF\Url::route(array('module' => 'default', 'controller' => 'profile', 'action' => 'edit')).'">click here</a>.', \DF\Flash::ERROR);
				return FALSE;
			}
			
			if (!$user->isActive())
			{
				$this->alert('<b>You have not met your requirements for dues or training.</b><br />You cannot sign up for events on the CARPOOL web site until you have completed the necessary dues and training requirements. Contact the organization leadership for more information.', \DF\Flash::ERROR);
				return FALSE;
			}
			
			return TRUE;
		}
	}
}