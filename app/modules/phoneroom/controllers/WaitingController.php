<?php
use \Entity\Ride;

class Phoneroom_WaitingController extends \CP\Controller\Action\Phoneroom
{	
	/**
	 * Main display.
	 */
    public function indexAction()
    {
		// Pull rides in database with this status.
        $rides = $this->em->createQuery('SELECT r FROM Entity\Ride r WHERE r.ndr_id = :ndr_id AND r.status = :status ORDER BY r.timetaken ASC')
            ->setParameters(array('ndr_id' => $this->_ndr_id, 'status' => 'waiting'))
            ->getArrayResult();
		
		array_walk($rides, array('\CP\PhoneRoom', 'processRide'));
		$this->view->rides = $rides;
	}
	
	public function assignAction()
	{
		$ride_num = (int)$_REQUEST['num'];
		$ride = Ride::find($ride_num);
		
		$ride->car = (int)$_REQUEST['car'];
		$ride->status = 'riding';
		$ride->timeassigned = new \DateTime('NOW');
		$ride->save();
		
		$this->_sendUpdate($ride);
		
		$this->redirectToRoute(array('module' => 'phoneroom', 'controller' => 'riding', 'action' => 'index', 'num' => $_REQUEST['num']));
		return;
	}
	
	public function splitAction()
	{
		$ride_num = intval($_REQUEST['num']);
		$ride = Ride::find($ride_num);
		
		if ($ride)
		{
			// Insert a duplicate ride into the database.
			$new_data = $ride->toArray();
			unset($new_data['num']);
			
            $new_data['is_confirmed'] = 0;
			$new_data['car'] = (int)$_REQUEST['car'];
			$new_data['riders'] = (int)$_REQUEST['riders'];
			$new_data['status'] = 'riding';
			$new_data['timeassigned'] = new \DateTime('NOW');
			
			$new_ride = new Ride;
            $new_ride->fromArray($new_data);
			$new_ride->save();

			$this->_sendUpdate($ride, 'change');
			$this->_sendUpdate($new_ride, 'add');
			
			$ride->riders = $ride['riders'] - $new_data['riders'];
			$ride->save();
		}
		
		$this->redirectToRoute(array('module' => 'phoneroom', 'controller' => 'riding', 'action' => 'index', 'num' => $ride_num));
		return;
	}
}