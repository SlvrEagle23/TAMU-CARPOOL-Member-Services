<?php
use \Entity\Settings;

class Directors_SettingsController extends \DF\Controller\Action
{
    public function permissions()
    {
        return \DF\Acl::getInstance()->isAllowed('access director features');
    }
    
	public function indexAction()
	{
		$form = new \DF\Form($this->config->forms->settings->form);
        
        $existing_settings = Settings::fetchArray();
        $form->setDefaults($existing_settings);
		
		if (!empty($_POST) && $form->isValid($_POST))
		{
            $data = $form->getValues();
			
            foreach($data as $key => $value)
            {
                Settings::setSetting($key, $value);
            }			
            
			$this->alert('Settings updated!');
			$this->redirectHere();
		}
		
		$this->view->form = $form;
	}
}