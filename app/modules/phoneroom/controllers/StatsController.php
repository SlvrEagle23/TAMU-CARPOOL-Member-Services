<?php
class Phoneroom_StatsController extends \CP\Controller\Action\Phoneroom
{	
	/**
	 * Main display.
	 */
    public function indexAction()
    {
		$db = $this->em->getConnection();
		
		$google_chart_base = 'http://chart.apis.google.com/chart';
		$stats = array();
		
		/**
		 * Rides by Hour
		 */
		
		$normal_counts = array();
		$ng_counts = array();
		
		$status_counts = array();
		$status_counts_overall = array();
		
		$status_to_include = array('done','cancelled','cpmissed');
		$hours_to_chart = array(23, 0, 1, 2, 3, 4);

		foreach($hours_to_chart as $hour)
		{
			// Total Northgate riders.
			$ng_count = (int)$db->fetchColumn('SELECT SUM(riders) AS max FROM rides WHERE ndr_id = ? AND status = ? AND pickup = ? AND HOUR(timedone) = ?', array($this->_ndr_id,'done','ng',$hour));
			$ng_counts[] = $ng_count;
			
			// Total regular riders.
			$normal_count = (int)$db->fetchColumn('SELECT SUM(riders) AS max FROM rides WHERE ndr_id = ? AND status = ? AND pickup != ? AND HOUR(timedone) = ?', array($this->_ndr_id,'done','ng',$hour));
			$normal_counts[] = $normal_count;
			
			// Riders per status type.
            $riders_per_status_raw = $db->fetchAll('SELECT status, SUM(riders) AS max FROM rides WHERE ndr_id = ? AND HOUR(timedone) = ? GROUP BY status', array($this->_ndr_id,$hour));
			$riders_per_status = array();
			foreach($riders_per_status_raw as $status)
			{
				$riders_per_status[$status['status']] = $status['max'];
			}
			
			foreach($status_to_include as $status)
			{
				$status_count = ($riders_per_status[$status]) ? $riders_per_status[$status] : 0.0;
				
				$status_counts[$status][] = $status_count;
				$status_counts_overall[$status] += $status_count;
			}
		}

		$type_by_hour = array(
			'chtt'		=> 'Type by Hour',
			'cht'		=> 'lc',
			'chd'		=> 't:'.implode(',', $normal_counts).'|'.implode(',', $ng_counts),
			'chxt'		=> 'x,y',
			'chxl'		=> '0:|11pm|12am|1am|2am|3am|4am|1:|1|50|100',
			'chs'		=> '300x180',
			'chco'		=> 'cc33ffcc,66cccccc',
			'chts'		=> '333333,18',
			'chdl'		=> 'Calls|Circuit',
		);
		$stats['type_by_hour'] = $this->saveChart('type_by_hour', $google_chart_base.'?'.http_build_query($type_by_hour));

		$status_by_hour = array(
			'chtt'		=> 'Rides by Hour',
			'cht'		=> 'lc',
			'chd'		=> 't:'.implode(',', $status_counts['done']).'|'.implode(',', $status_counts['cancelled']).'|'.implode(',', $status_counts['cpmissed']),
			'chxt'		=> 'x,y',
			'chxl'		=> '0:|11pm|12am|1am|2am|3am|4am|1:|1|50|100',
			'chs'		=> '300x180',
			'chco'		=> '00ff00cc,0000ffcc,ff0000cc',
			'chts'		=> '333333,18',
			'chdl'		=> 'Done|Cancelled|Missed',
		);
		$stats['status_by_hour'] = $this->saveChart('status_by_hour', $google_chart_base.'?'.http_build_query($status_by_hour));

		/**
		 * Rides Per Car
		 */
		
		// Get current highest car number.
		$highest_car_num = (int)$db->fetchColumn('SELECT MAX(car) AS max FROM rides WHERE ndr_id = ?', array($this->_ndr_id));
		$num_cars = max($highest_car_num, $this->config->carpool->default_num_cars);

		$ng_counts = array();
		$normal_counts = array();
		$all_counts = array();
		$car_nums = array();
		
		for($i = 1; $i <= $num_cars; $i++)
		{
			$car_nums[] = $i;
			
			// Completed NG rides
            $ng_count = (int)$db->fetchColumn('SELECT SUM(riders) AS max FROM rides WHERE ndr_id = ? AND status = ? AND pickup = ? AND car = ?', array($this->_ndr_id, 'done', 'ng', $i));
			$ng_counts[] = $ng_count;
			
			// Completed normal rides
            $normal_count = (int)$db->fetchColumn('SELECT SUM(riders) AS max FROM rides WHERE ndr_id = ? AND status = ? AND pickup != ? AND car = ?', array($this->_ndr_id, 'done', 'ng', $i));
			$normal_counts[] = $normal_count;
			
			$all_count = $normal_count + $ng_count;
			$all_counts[] = 't'.$all_count.',444444,0,'.($i-1).',13,1';
		}	

		$size_factor = round($num_cars*(-21.5/18)+44);
		
		$rides_by_car = array(
			'chtt'		=> 'Rides by Type for each Car',
			'cht'		=> 'bvs',
			'chd'		=> 't:'.implode(',', $normal_counts).'|'.implode(',', $ng_counts),
			'chs'		=> '640x200',
			'chts'		=> '333333,18',
			'chco'		=> 'cc33ffcc,66cccccc',
			'chxt'		=> 'x,y',
			'chxl'		=> '0:|'.implode('|', $car_nums).'|1:|0|20|40',
			'chds'		=> '0,40',
			'chbh'		=> $size_factor,
			'chdl'		=> 'Calls|Circuit',
			'chm'		=> implode('|', $all_counts),
		);
		$stats['rides_by_car'] = $this->saveChart('rides_by_car', $google_chart_base.'?'.http_build_query($rides_by_car));

		/**
		 * Overall Rides by Status
		 */
		
		$done_rides = $status_counts_overall['done'];
		$cancelled_rides = $status_counts_overall['cancelled'];
		$missed_rides = $status_counts_overall['cpmissed'];
		
		$total_rides = $done_rides + $cancelled_rides + $missed_rides;

		$g_int = round($total_rides / 4);
		$g_int2 = $g_int * 2;
		$g_int3 = $g_int * 3;
		$g_int4 = $g_int * 4;

		$rides_by_status = array(
			'cht'		=> 'bvg',
			'chd'		=> 't:'.$done_rides.'|'.$cancelled_rides.'|'.$missed_rides,
			'chs'		=> '200x170',
			'chco'		=> '00ff00cc,0000ffcc,ff0000cc',
			'chts'		=> '00ff00,16',
			'chdl'		=> 'Done|Cancelled|Missed',
			'chxt'		=> 'y',
			'chxl'		=> '1:|0|'.$g_int.'|'.$g_int2.'|'.$g_int3.'|'.$g_int4,
			'chds'		=> '0,'.$g_int4,
			'chbh'		=> '25',
			'chm'		=> 't'.$done_rides.',444444,0,0,13,1|t'.$cancelled_rides.',444444,1,0,13,1|t'.$missed_rides.',444444,2,0,13,1',
		);
		$stats['rides_by_status'] = $this->saveChart('rides_by_status', $google_chart_base.'?'.http_build_query($rides_by_status));

		/**
		 * Pie Chart
		 */
		
		$rides_by_location_raw = $db->fetchAll('SELECT location, SUM(riders) AS total FROM rides WHERE ndr_id = ? GROUP BY location', array($this->_ndr_id));
		$rides_by_location = array();
		
		foreach($rides_by_location_raw as $location)
		{
			$rides_by_location[$location['location']] = $location['total'];
		}
		
		$city_totals = array();
		$city_names = array();
		
		foreach($this->config->carpool->cities->toArray() as $city_key => $city_name)
		{
			$city_totals[] = ($rides_by_location[$city_key]) ? $rides_by_location[$city_key] : 0.0;
			$city_names[] = urlencode($city_name);
		}

		$pie_chart = array(
			'cht'		=> 'p',
			'chd'		=> 't:'.implode(',', $city_totals),
			'chs'		=> '200x100',
			'chco'		=> 'cccccccc,aa4444ee',
			'chdl'		=> implode('|', $city_names),
		);
		$stats['pie_chart'] = $this->saveChart('pie_chart', $google_chart_base.'?'.http_build_query($pie_chart));

		/**
		 * Ride Meter
		 */
		
		$ride_time_sum = 0;
		$ride_time_num = 0;
		
		$rides = $db->fetchAll('SELECT timedone, timeassigned FROM rides WHERE ndr_id = ? AND status = ? AND pickup != ?', array($this->_ndr_id, 'done', 'ng'));

		foreach($rides as $ride)
		{
			$ride_time_num++;
			
			$ride_time_start = strtotime($ride['timeassigned']);
			$ride_time_end = strtotime($ride['timedone']);
			
			$ride_time_seconds = $ride_time_end - $ride_time_start;
			$ride_time = round($ride_time_seconds / 60, 0);
			
			$ride_time_sum += $ride_time;
		}
		
		if ($ride_time_num != 0)
			$average_ride_time = round($ride_time_sum / $ride_time_num, 0);
		else
			$average_ride_time = 0;

		$ride_meter = array(
			'cht'		=> 'gom',
			'chd'		=> 't:'.$average_ride_time,
			'chs'		=> '220x110',
			'chco'		=> '00ff00,ffff00,ff0000',
			'chtt'		=> 'Average Wait',
			'chts'		=> '333333,18',
			'chl'		=> $average_ride_time.' mins',
		);
		$stats['ride_meter'] = $this->saveChart('ride_meter', $google_chart_base.'?'.http_build_query($ride_meter));

		$this->view->stats = $stats;
	}

	protected function saveChart($chart_name, $chart_url)
	{	
		$chart_file_base = 'charts/chart_'.$chart_name.'.jpg';
		
		$chart_filename = DF_INCLUDE_STATIC.DIRECTORY_SEPARATOR.$chart_file_base;
		$chart_file_url = \DF\Url::content($chart_file_base);
		
		if (!file_exists($chart_filename) || filemtime($chart_filename) < (time() - 600))
		{
			$chart_image = file_get_contents($chart_url);
			file_put_contents($chart_filename, $chart_image);
		}
		
		return $chart_file_url;
	}
}