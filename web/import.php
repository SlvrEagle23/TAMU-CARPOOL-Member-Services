<?php
require_once dirname(__FILE__) . '/../app/bootstrap.php';
$application->bootstrap();

exit;

$db = new Zend_Db_Adapter_Pdo_Mysql(array(
    'host'     => '127.0.0.1',
    'username' => 'carpool',
    'password' => 'p4ac9ecU',
    'dbname'   => 'archive_carpool_2010'
));

// User lookup.
$user_lookup = array();

$new_users_raw = User::fetchArray();
$new_users = array();
foreach($new_users_raw as $user)
{
	$new_users[$user['uin']] = $user['id'];
}

$old_users = $db->fetchAll('SELECT * FROM users');
foreach($old_users as $old_user)
{	
	$old_user_id = $old_user['user_id'];
	
	if (isset($new_users[$old_user['uin']]))
	{
		$user_lookup[$old_user_id] = $new_users[$old_user['uin']];	
	}
}

// Pull NDRs.
$ndrs = Ndr::fetchAll();

foreach($ndrs as $ndr)
{
	$ndr_data = Zend_Json::decode($ndr->ndr_data);
	
	$positions = array();
	$positions_raw = explode('|', $ndr_data['assignment']['results']);
	
	if ($positions_raw)
	{
		$new_positions = array();
		
		foreach($positions_raw as $position)
		{
			list($user_id, $position_name) = explode(":", $position);
			
			if (isset($user_lookup[$user_id]))
				$new_positions[] = $user_lookup[$user_id].':'.$position_name;
		}
		
		$new_positions_raw = implode('|', $new_positions);
		
		if ($new_positions_raw)
		{
			$ndr_data['assignment']['results'] = $new_positions_raw;
			$ndr->ndr_data = Zend_Json::encode($ndr_data);
			$ndr->save();	
		}
	}
}

echo 'Done';