<?php
use \Entity\Team;
use \Entity\Role;
use \Entity\User;

class Members_ReportController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::isAllowed('access exec features');
	}
	
	public function indexAction()
	{
        $teams_raw = Team::fetchArray();
        $teams = array();
        
        foreach($teams_raw as $team_raw)
        {
            if ($team_raw['team_name'])
                $teams[$team_raw['team_id']] = $team_raw['team_name'];
        }
            
        if (!$this->_hasParam('team'))
        {
            $this->view->Team = $teams;
            $this->render('select');
            return;
        }
        else
        {
            $team = (int)$this->_getParam('team');
            $team_name = $teams[$team];
            
            $all_users = $this->em->createQuery('SELECT u, r, t FROM \Entity\User u JOIN u.roles r JOIN u.team t WHERE r.id IS NOT NULL AND t.team_id = :team_id ORDER BY t.team_name ASC, u.lastname ASC, u.firstname ASC')
                ->setParameter('team_id', $team)
                ->getArrayResult();
            
			foreach($all_users as &$user_row)
			{
				$user_row['team_name'] = $team_name;
				$user_row['group_name'] = (int)$user_row['roles'][0]['name'];
				$user_row['statistics'] = User::getUserNightsAndPoints($user_row['id'], $user_row['uin']);
			}
            
            $this->view->team = $team;
            $this->view->team_name = $team_name;
            $this->view->users = $all_users;
        }
	}
    
    public function exportAction()
    {
        $team = (int)$this->_getParam('team');
        $team_name = $Team[$team];
        
        $all_users = $this->em->createQuery('SELECT u, r, t FROM \Entity\User u JOIN u.roles r JOIN u.team t WHERE r.id IS NOT NULL ORDER BY t.team_name ASC, u.lastname ASC, u.firstname ASC')
            ->getArrayResult();
        
        foreach($all_users as &$user_row)
        {
            $user_row['team_name'] = $user_row['team']['team_name'];
            $user_row['group_name'] = (int)$user_row['roles'][0]['name'];
            $user_row['statistics'] = User::getUserNightsAndPoints($user_row['id'], $user_row['uin']);
        }
        
        $export_data = array(
            array(
                'Name',
                'Team',
                'UIN',
                'E-mail Address',
                'Phone Number',
                'Shirt Size',
                'Emergency 1 Name',
                'Emergency 1 Phone',
                'Emergency 2 Name',
                'Emergency 2 Phone',
                'Parent Phone',
                'Parent Address',
                'Nights',
                'Current Points',
                'Projected Points',
            )
        );
            
        foreach($all_users as $user)
        {
            $export_row = array(
                $user['lastname'].', '.$user['firstname'],
                $user['team_name'],
                $user['uin'],
                $user['email'],
                $user['cphone'],
                $user['shirt_size'],
                $user['emer1_name'],
                $user['emer1_phone'],
                $user['emer2_name'],
                $user['emer2_phone'],
                $user['pphone'],
                $user['pstreet'].' - '.$user['pcity'].', '.$user['pstate'].' '.$user['pzip'],
                (int)$user['statistics']['nights']['total_num'],
                (int)$user['statistics']['points']['current'],
                (int)$user['statistics']['points']['projected'],
            );
            $export_data[] = $export_row;
        }
        
        $this->doNotRender();
        \DF\Export::csv($export_data);
        return;
    }
}