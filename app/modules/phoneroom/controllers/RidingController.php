<?php
use \Entity\Ride;

class Phoneroom_RidingController extends \CP\Controller\Action\Phoneroom
{	
	/**
	 * Main display.
	 */
    public function indexAction()
    {
        // Pull rides in database with this status.
        $rides = $this->em->createQuery('SELECT r FROM Entity\Ride r WHERE r.ndr_id = :ndr_id AND r.status = :status ORDER BY r.car ASC')
            ->setParameters(array('ndr_id' => $this->_ndr_id, 'status' => 'riding'))
            ->getArrayResult();
		
		array_walk($rides, array('\CP\PhoneRoom', 'processRide'));
		$this->view->rides = $rides;
	}
	
	public function undoAction()
	{
		$ride_num = intval($this->_getParam('num'));
		$ride = Ride::find($ride_num);
		
		if ($ride)
		{
			$ride->status = 'waiting';
			$ride->car = 0;
			$ride->save();

			$this->_sendUpdate($ride, 'status');
		}
		
		$this->redirectToRoute(array('module' => 'phoneroom', 'controller' => 'riding', 'action' => 'index', 'num' => $ride_num));
		return;
	}
	
	public function doneAction()
	{
		$ride_num = intval($this->_getParam('num'));
		$ride = Ride::find($ride_num);
		
		$ride->status = 'done';
		$ride->timedone = new \DateTime('NOW');
		$ride->save();

		$this->_sendUpdate($ride, 'status');
		
		$this->redirectToRoute(array('module' => 'phoneroom', 'controller' => 'waiting', 'action' => 'index'));
		return;
	}
}