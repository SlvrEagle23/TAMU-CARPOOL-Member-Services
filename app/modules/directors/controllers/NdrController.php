<?php
use \Entity\Event;
use \Entity\Ndr;
use \Entity\User;

class Directors_NdrController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::getInstance()->isAllowed('access director features');
	}

	public function indexAction()
	{
        // New Report options.
        $event_forward_threshold = time() + (86400 * 5);
        $event_backward_threshold = time() - (86400 * 5);
        
        $events_without_reports_raw = $this->em->createQuery('SELECT e FROM \Entity\Event e LEFT JOIN e.ndr n WHERE e.category IN (:categories) AND n IS NULL AND e.event_date BETWEEN :date_start AND :date_end ORDER BY e.event_date ASC')
            ->setParameters(array(
                'categories'        => array('night'),
                'date_start'        => date('Y-m-d', $event_backward_threshold),
                'date_end'          => date('Y-m-d', $event_forward_threshold),
            ))->getArrayResult();
        
        $events_without_reports = array();
        foreach((array)$events_without_reports_raw as $event)
        {
            $events_without_reports[$event['num']] = $event['event_date']->format('m/d/Y').': '.$event['title'];
        }
        
        $this->view->events_without_reports = $events_without_reports;
        
        // Get archived reports.
        $reports = $this->em->createQueryBuilder()
            ->select('n, e')
            ->from('\Entity\Ndr', 'n')
            ->join('n.event', 'e')
            ->orderBy('e.event_date', 'DESC');
        
        $paginator = new \DF\Paginator\Doctrine($reports);
        $paginator->setCurrentPageNumber(($this->_hasParam('page')) ? $this->_getParam('page') : 1);
		$this->view->pager = $paginator;
	}
    
    public function toggleAction()
    {
        $ndr_id = (int)$this->_getParam('id');
        $ndr = Ndr::find($ndr_id);
        
        $ndr->is_archived = 1 - $ndr->is_archived;
        $ndr->save();
        
        $this->alert('Report status toggled.');
		
		$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
		return;
    }
	
	public function addAction()
	{
        // Check for an event with the ID specified.
        $event_id = (int)$this->_getParam('event_id');
        $event = Event::find($event_id);
        $report = $event->ndr;
        
        if ($report instanceof Ndr)
        {
            $this->redirectFromHere(array('action' => 'edit', 'id' => $report->id));
        }
        else
        {
            $date_raw = $event['event_date']->getTimestamp();
            
            $report = new Ndr;
            $report->event = $event;
            $report->ndr_date = new \DateTime('@'.$date_raw);
            
            // Check for a report from yesterday (to preload data).
            $yesterday = new \DateTime('@'.($date_raw-86400));
            $yesterday_report = Ndr::fetchByDate($yesterday);
            
            if ($yesterday_report instanceof Ndr)
            {
                $yesterday_data = $yesterday_report['ndr_data'];
                
                if ($yesterday_data['cars'])
                {
                    $new_ndr_data = array('cars' => array());
                    
                    foreach($yesterday_data['cars'] as $car_offset => $car_info)
                    {
                        unset($car_info['notes']);
                        unset($car_info['couch']);
                        $new_ndr_data['cars'][$car_offset] = $car_info;
                    }
                    
                    $report->ndr_data = $new_ndr_data;
                }
            }
            
            $report->save();
            
            $ndr_id = $report->id;
            
            $this->alert('New NDR created!');
            $this->redirectFromHere(array('action' => 'edit', 'id' => $ndr_id));
            return;
        }
	}

	public function editAction()
	{
		$user = \DF\Auth::getInstance()->getLoggedInUser();
		
		$ndr_id = (int)$this->_getParam('id');
		$report = Ndr::find($ndr_id);
        
		if (!$report)
		{
			$this->alert('NDR not found!');
			$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
			return;
		}
		
		$report_info = $report->toArray();
		$this->view->report = array_merge($report_info, (array)$report_info['ndr_data']);
        
		// If the NDR is for the current night, restrict access to the current DIC (if assigned).
		$read_only_mode = FALSE;
		if ($report->is_archived != 1 && !\DF\Acl::isAllowed('access director features'))
		{
			if (isset($report_info['ndr_data']['assignment']['results']))
			{
				$positions = array();
				$positions_raw = explode('|', $report_info['ndr_data']['assignment']['results']);
				foreach($positions_raw as $position)
				{
					list($user_id, $position_name) = explode(":", $position);
					$positions[$position_name][] = $user_id;
				}
				
				$night_directors = array_merge((array)$positions['dic'], (array)$positions['rover']);
				
				if (count($night_directors) > 0)
				{
					if (!in_array($user->id, $night_directors))
						$read_only_mode = TRUE;
				}
			}
		}
		
		if ($read_only_mode)
		{
			$this->alert('This NDR is currently in "Read Only" mode. You cannot save changes to this report, because the night is currently in progress and another director is listed as the DIC for the night.', \DF\Flash::ERROR);	
		}
		$this->view->read_only_mode = $read_only_mode;
		
		// Attempt to find a night associated with the same date as this NDR.
        $night = $report->event;
		
		if (!$night)
			throw new Exception('No night could be found that corresponds with this NDR report. You must create an event associated with this date before managing an NDR report for it.');
        
		$this->view->night = $night->toArray();
		
		if (isset($_REQUEST['report']))
		{
			// Handle data submission and redirection.
            $update_status_query = $this->em->createQuery('UPDATE \Entity\Signup s SET s.status=:new_status WHERE s.user_id = :user_id AND s.event_id = :event_id')
                ->setParameter('event_id', $night->num);
            
			foreach((array)$_REQUEST['report']['home']['members'] as $user_id => $status_text)
			{
				if ($status_text == "ontime")
					$status = CP_SIGNUP_ONTIME;
				else if ($status_text == "late")
					$status = CP_SIGNUP_LATE;
				else if ($status_text == "noshow")
					$status = CP_SIGNUP_NOSHOW;
				else if ($status_text == "bonus")
					$status = CP_SIGNUP_BONUSPOINTS;
				else
					$status = CP_SIGNUP_UNCONFIRMED;
				
                $update_status_query->setParameters(array(
                    'user_id'   => $user_id,
                    'new_status' => $status,
                ))->execute();
			}
			
			$report->ndr_data = $_REQUEST['report'];
			$report->save();
			
			$this->alert('NDR Saved - '.date('g:ia'));
			
			if (isset($_REQUEST['active_tab']))
				$this->redirect($_REQUEST['active_tab']);
			else
				$this->redirect('#home');
			return;
		}
		else
		{
            // Get any debriefings associated with the report.
            $debriefings_raw = $report->debriefings;
            
			$debriefings = array();
			foreach($debriefings_raw as $debriefing)
			{
                $debriefing_data = $debriefing->debriefing_data;
                $debriefing_info = $debriefing->toArray() + (array)$debriefing_data;
                
				$debriefings[$debriefing['user_id']] = $debriefing_info;
			}
            
			// View members who signed up for the night associated with this report.
			$members_signed_up_raw = $this->em->createQuery('SELECT u, s, r FROM Entity\Signup s JOIN s.user u LEFT JOIN u.roles r WHERE s.event_id = :event_id ORDER BY u.lastname ASC, u.firstname ASC, u.username ASC')
                ->setParameter('event_id', $night->num)
                ->getArrayResult();
            
			$members_signed_up = array();
			$sms_recipients = array();
			$email_recipients = array();
			
			foreach($members_signed_up_raw as $member)
			{
                $member = array_merge($member, $member['user']);
                
				$is_member = (stristr($member['roles'][0]['name'], 'Member') !== FALSE);
				$member['user']['avatar'] = User::getUserAvatar($member['uin']);
				$member['user']['name'] = $member['firstname'].' '.$member['lastname'].' ('.$member['group_name'].')';
                
                if (isset($debriefings[$member['id']]))
                {
                    $member['is_debriefed'] = TRUE;
                    $member['debriefing'] = $debriefings[$member['id']];
                }
                else
                {
                    $member['is_debriefed'] = FALSE;
                }
                
				$member_desc = array();
				if ($member['sex'] == "M")
					$member_desc[] = $this->view->icon(array('image' => 'user', 'title' => 'Male'));
				else if ($member['sex'] == "F")
					$member_desc[] = $this->view->icon(array('image' => 'user_female', 'title' => 'Female'));
				else
					$member_desc[] = $this->view->icon(array('image' => 'user_green', 'title' => 'No Gender Set'));
				
				if (!$is_member)
					$member_desc[] = $this->view->icon(array('image' => 'flag_green', 'title' => 'Exec'));
				else if ((int)$member['semesters'] <= 1)
					$member_desc[] = $this->view->icon(array('image' => 'asterisk_yellow', 'title' => 'New'));
				
				$member['description'] = implode('', $member_desc);
				
				if (stristr($member['roles'][0]['name'], 'Member') !== FALSE)
				{
					$members_signed_up['members'][] = $member;		
					$gender = ($member['sex'] == "M") ? "M" : "F";
					$members_signed_up[$gender][] = $member;
				}
				else if (stristr($member['roles'][0]['name'], 'Advisor') !== FALSE)
				{
					$members_signed_up['advisors'][] = $member;
				}
				else
				{
					$members_signed_up['execs'][] = $member;
				}
				$members_signed_up['all'][] = $member;
				
				$sms_recipients[] = $member['id'];
				$email_recipients[] = $member['email'];
			}
			
			$this->view->email_recipients = implode('; ', $email_recipients);
			$this->view->sms_recipients = implode(',', $sms_recipients);
			$this->view->members_signed_up = $members_signed_up;
		}

		// PubNub integration.
		$this->view->pubnub_channel = 'tamu_carpool_updates_'.$report->id;
	}
	
	/**
	 * Specialized function pages
	 */
	
	// View all IT notes.
	public function itnotesAction()
	{
		$all_ndrs = Doctrine_Query::create()
			->from('Ndr n')
			->orderBy('n.ndr_date DESC')
			->fetchArray();
		
		$ndrs_with_notes = array();
		foreach($all_ndrs as $ndr)
		{
			$report = Zend_Json::decode($ndr['ndr_data']);
			
			if ($report['notes']['it'])
			{
				$ndrs_with_notes[] = $ndr+$report;
			}
		}
		
		$this->view->ndrs = $ndrs_with_notes;
	}
    
    // Send e-mails to couches for review.
    public function mailcouchesAction()
    {
        $this->doNotRender();
        
        $return = array();
        
        $ndr_id = intval($this->_getParam('id'));
		$report = Ndr::fetchById($ndr_id);
        
        $night = $report->Event;

        // View members who signed up for the night associated with this report.
        $members_signed_up_raw = Doctrine_Query::create()
            ->from('User u')
            ->innerJoin('u.Signup s')
            ->innerJoin('u.Roles r')
            ->addWhere('s.event_id = ?', $night['num'])
            ->orderBy('u.lastname, u.firstname, u.username')
            ->fetchArray();
        
        $members_by_id = array();
        $member_select = array();
        foreach($members_signed_up_raw as $member)
        {
            $member_name = $member['lastname'].', '.$member['firstname'].' ('.$member['Roles'][0]['name'].')';
            $members_select[$member['id']] = $member_name;
            $members_by_id[$member['id']] = $member;
        }
        
        // Get any debriefings associated with the report.
        $debriefings_raw = $report->Debriefings;
        
        $debriefings_by_couch = array();
        foreach($debriefings_raw as $debriefing)
        {
            $debriefing_data = $debriefing['debriefing_data'];
            
            if ($debriefing_data['CouchName'])
            {
                $couch_id = array_search($debriefing_data['CouchName'], $members_select);
                
                if ($couch_id)
                {
                    $debriefings_by_couch[$couch_id][] = $debriefing_data;
                }
            }
        }
        
        if ($debriefings_by_couch)
        {
            $notified_users = array();
            
            foreach($debriefings_by_couch as $user_id => $debriefings)
            {
                // Get user information.
                $user = $members_by_id[$user_id];
                
                // Get average score.
                $debriefing_scores = array();
                foreach($debriefings as $debriefing)
                {
                    $debriefing_scores[] = (int)$debriefing['CouchScore'];
                }
                $average_score = number_format(array_sum($debriefing_scores) / count($debriefing_scores), 1);
                
                // Send report.
                \DF\Messenger::send(array(
                    'to'        => $user['email'],
                    'subject'   => 'NDR Couch Score Report',
                    'template'  => 'ndr_couchscore',
                    'vars'      => array(
                        'user' => $user,
                        'debriefings' => $debriefings,
                        'average_score' => $average_score,
                        'report' => $report,
                    ),
                ));
                
                $notified_users[] = $user['firstname'].' '.$user['lastname'].' ('.$user['email'].')';
            }
            
            $return['result'] = "Sent notifications to:\n".implode("\n", $notified_users);
        }
        else
        {
            $return['result'] = "No debriefings were listed to be notified.";
        }
        
        echo Zend_Json::encode($return);
        return;
    }

    // Auto-save notes.
    public function savenotesAction()
    {
    	$this->doNotRender();

    	$ndr_id = (int)$this->_getParam('id');
		$report = Ndr::find($ndr_id);

		$ndr_data = $report->ndr_data;
		$ndr_data['notes']['notes'] = $_REQUEST['notes'];
		$report->ndr_data = $ndr_data;
		$report->save();

		echo \Zend_Json::encode(array(
			'status'		=> 'success',
			'message'		=> 'Notes saved successfully at '.date('g:ia'),
		));
		return;
    }
	
	// Pull data from the phoneroom.
	public function phoneroomAction()
	{
		$this->doNotRender();
        
        $ndr_id = intval($this->_getParam('id'));
		$report = Ndr::find($ndr_id);
        
        $rides = $report->rides;
		
		$type_sums = array();
		foreach($rides as $ride)
		{
			$type_sums[$ride['status']] += (int)$ride['riders'];
			
			if ($ride['pickup'] == "ng")
				$type_sums['ng'] += (int)$ride['riders'];
		}
		
		echo \Zend_Json::encode($type_sums);
		return;
	}
}