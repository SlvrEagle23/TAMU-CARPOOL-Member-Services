<?php
namespace Entity;


/**
 * NiceThing
 *
 * @Table(name="nice_thing")
 * @Entity
 * @HasLifecycleCallbacks
 */
class NiceThing extends \DF\Doctrine\Entity
{
	public function __construct()
    {
        $this->created_at = $this->updated_at = new \DateTime("now");
    }
    
    /** @PreUpdate */
    public function updated()
    {
        $this->updated_at = new \DateTime("now");
    }
    
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;

    /** @Column(name="submitter_id", type="integer") */
    protected $submitter_id;

    /** @Column(name="message", type="text") */
    protected $message;

    /** @Column(name="created_at", type="datetime") */
    protected $created_at;
    
    /** @Column(name="updated_at", type="datetime") */
    protected $updated_at;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="nice_things")
     * @JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="nice_things_submitted")
     * @JoinColumn(name="submitter_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $submitter;
}