<?php
return array(
    'default' => array(
		'directors'		=> array(
			// Director access pages.
			'label'		=> 'Directors',
			'module'	=> 'directors',
			'permission' => 'access director features',
			'order'		=> 90,
			'pages'		=> array(
				'settings'		=> array(
					'label'			=> 'Site Settings',
					'module'		=> 'directors',
					'controller'	=> 'settings',
					'action'		=> 'index',
					'permission' 	=> 'access director features',
				),
				'events'		=> array(
					'label'			=> 'Manage Events',
					'module'		=> 'directors',
					'controller'	=> 'events',
					'action'		=> 'index',
					'permission' 	=> 'access director features',
					'pages'			=> array(
						'events_edit'	=> array(
							'module'	=> 'directors',
							'controller' => 'events',
							'action'	=> 'edit',
							'permission' => 'access director features',
						),
					),
				),
				'teams'			=> array(
					'label'			=> 'Manage Teams',
					'module'		=> 'directors',
					'controller'	=> 'teams',
					'action'		=> 'index',
					'permission' 	=> 'access director features',
					'pages'			=> array(
						'team_edit'	=> array(
							'module'	=> 'directors',
							'controller' => 'teams',
							'action'	=> 'edit',
							'permission' => 'access director features',
						),
						'team_members' => array(
							'module'	=> 'directors',
							'controller' => 'teams',
							'action'	=> 'members',
							'permission' => 'access director features',
						),
					),
				),
				'ndr'			=> array(
					'label'			=> 'NDR',
					'module'		=> 'directors',
					'controller'	=> 'ndr',
					'action'		=> 'index',
					'permission' 	=> 'access director features',
					'pages'			=> array(
						'ndr_edit'		=> array(
							'module'		=> 'directors',
							'controller'	=> 'ndr',
							'action'		=> 'edit',
							'permission' 	=> 'access director features',
						),
					),
				),
				
				'reset'			=> array(
					'label'			=> 'Reset Site',
					'module'		=> 'directors',
					'controller'	=> 'reset',
					'action'		=> 'index',
					'permission' 	=> 'access director features',
				),
			),
		),
    ),
);