<?php
use \Entity\Ndr;
use \Entity\NdrDebriefing;
use \Entity\User;

class Executives_DebriefingController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::getInstance()->isAllowed('access exec features');
	}

	public function indexAction()
	{
        $ndr_id = (int)$this->_getParam('ndr_id');
        
        if ($ndr_id == 0)
        {
            $active_ndrs = Ndr::fetchActive();
            $this->view->active_ndrs = $active_ndrs;
            
            $this->render('select');
            return;
        }
        
        $ndr = Ndr::find($ndr_id);
        
		if (isset($_REQUEST['form']))
		{
            $user_id = (int)$this->_getParam('id');
            
            $delete_query = $this->em->createQuery('DELETE FROM \Entity\NdrDebriefing nd WHERE nd.ndr_id = :ndr_id AND nd.user_id = :user_id')
                ->setParameter('ndr_id', $ndr_id)
                ->setParameter('user_id', $user_id)
                ->execute();
			
			$debriefing = new NdrDebriefing();
			$debriefing->ndr = $ndr;
			$debriefing->user = User::find($user_id);
			$debriefing->debriefing_data = $_REQUEST['form'];
            $debriefing->is_high_priority = $_REQUEST['is_high_priority'];
			$debriefing->save();
			
			$this->alert('Debriefing submitted. You can submit another debriefing if needed.');
			$this->redirectFromHere(array('id' => NULL));
			return;
		}
        
        $night = $ndr->event;
        $this->view->night = $night = $night->toArray();
        
        // Pull NDR and look for custom question.
        $ndr_data = $ndr->ndr_data;
        $this->view->custom_question = $ndr_data['debrief']['question'];
        
        // Get any debriefings associated with the report.
        $debriefings_raw = $ndr->debriefings;
        
        $debriefings = array();
        foreach($debriefings_raw as $debriefing)
        {
            $debriefings[] = $debriefing['user_id'];
        }

        // View members who signed up for the night associated with this report.
        $members_signed_up_raw = $this->em->createQuery('SELECT s, u, r FROM Entity\Signup s JOIN s.user u JOIN u.roles r WHERE s.event_id = :event_id ORDER BY u.lastname ASC, u.firstname ASC, u.username ASC')
            ->setParameter('event_id', $night['num'])
            ->getArrayResult();
        
        $members_signed_up = array();
        
        $members_select = array();
        $members_needing_debriefing = array();
        $members_select_by_name = array(
            ''		=> '(None)',
        );
        
        foreach($members_signed_up_raw as $member)
        {
            $member_name = $member['user']['lastname'].', '.$member['user']['firstname'].' ('.$member['user']['roles'][0]['name'].')';
            
            $members_select_by_name[$member_name] = $member_name;
            $members_select[$member['user']['id']] = $member_name;
            
            if (!in_array($member['user']['id'], $debriefings))
                $members_needing_debriefing[$member['user']['id']] = $member_name;
        }
        
        if ($this->_hasParam('id'))
        {
            $this->view->user_id = $user_id = $this->_getParam('id');
            $this->view->member = $members_select[$user_id];
            
            $this->view->members_select_by_name = $members_select_by_name;
            $this->view->members_select = $members_select;
            $this->render('submit');
            return;
        }
        else
        {
            $this->view->members = $members_needing_debriefing;
            $this->render();
            return;
        }
	}
}