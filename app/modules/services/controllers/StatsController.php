<?php
class Services_StatsController extends \DF\Controller\Action
{
	public function permissions()
	{
		return true;
	}
	
	/**
	 * Main display.
	 */
    public function indexAction()
    {
		\Zend_Layout::getMvcInstance()->setLayout('compact');
		
		// Send required information for the site's layout to the template engine.
        $today_rides = $this->em->createQuery('SELECT r FROM \Entity\Ride r WHERE r.ridedate = :ride_date')
            ->setParameter('ride_date', ADJUSTED_TIMESTAMP_MYSQL)
            ->getArrayResult();

		$type_sums = array();
		foreach((array)$today_rides as $ride)
		{
			$type_sums[$ride['status']]['riders'] += (int)$ride['riders'];
			$type_sums[$ride['status']]['rides'] += 1;
		}
		
		$this->view->type_sums = $type_sums;
	}
}