<?php
return array(
    'default' => array(
		// Events
		'events' => array(
			'label'		=> 'Events',
			'module'	=> 'events',
			'order'		=> 0,
			'permission' => 'access member features',
			'pages'		=> array(
				'event_index'		=> array(
					'module'		=> 'events',
					'controller'	=> 'event',
					'permission'	=> 'access member features',
				),
				'event_agreement'	=> array(
					'module'		=> 'events',
					'controller'	=> 'event',
					'action'		=> 'agreement',
					'permission'	=> 'access director features',
				),
			),
		),
    ),
);
