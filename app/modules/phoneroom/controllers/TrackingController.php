<?php
class Phoneroom_TrackingController extends \CP\Controller\Action\Phoneroom
{
	public function indexAction()
	{
		// Pull rides in database with this status.
        $rides = $this->em->createQuery('SELECT r FROM Entity\Ride r WHERE r.ndr_id = :ndr_id AND r.status = :status ORDER BY r.car ASC')
            ->setParameters(array('ndr_id' => $this->_ndr_id, 'status' => 'riding'))
            ->getArrayResult();
		
		array_walk($rides, array('\CP\PhoneRoom', 'processRide'));
		$this->view->rides = $rides;
	}
}