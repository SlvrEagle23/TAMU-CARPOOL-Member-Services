<?php
use \Entity\Ride;
use \Entity\Contacted;

class Phoneroom_CarsController extends \CP\Controller\Action\Phoneroom
{	
	/**
	 * Main display.
	 */
    public function indexAction()
    {
		$db = $this->em->getConnection();
        $result = $db->executeQuery('SELECT MAX(car) AS max FROM rides WHERE ndr_id = ?', array($this->_ndr_id));
		$results = $result->fetchAll();
        $highest_car_num = (int)$results[0]['max'];
        
        // Pull NDR and look for custom question.
        $ndr = $this->_ndr;
        $ndr_positions = $ndr->getPositions();
        $ndr_data = $ndr->ndr_data;
        
        $cars_in_ndr = array();
        foreach($ndr_data['cars'] as $num => $car_info)
        {
            if ($car_info['license'])
                $cars_in_ndr[] = $num;
        }
        
        if (empty($cars_in_ndr))
            $num_cars_in_ndr = 0;
        else
            $num_cars_in_ndr = max((array)$cars_in_ndr);
        $num_cars = max($num_cars_in_ndr, $highest_car_num, $this->config->carpool->default_num_cars);
		
		// Loop through the list of cars.
		$cars = array();
		for($i = 1; $i <= $num_cars; $i++)
		{
			$car = array();
			
			// Looks up the last action of done (will detect a circuit car)
            try
            {
                $done_result_raw = $this->em->createQuery('SELECT r FROM Entity\Ride r WHERE r.ndr_id = :ndr_id AND r.car = :car ORDER BY r.timedone DESC')
                    ->setMaxResults(1)
                    ->setParameters(array(
                        'ndr_id' => $this->_ndr_id,
                        'car' => $i
                    ))->getSingleResult();
                $done_result = $done_result_raw->toArray();
            }
            catch(Exception $e)
            {
                $done_result = array();
            }
            
            $car_done_time = $done_result['timedone'];
			$car_pickup = $done_result['pickup'];
			
			// Looks up the last action of done (will detect a circuit car)
            try
            {   
                $riding_result_raw = $this->em->createQuery('SELECT r FROM Entity\Ride r WHERE r.ndr_id = :ndr_id AND r.car = :car ORDER BY r.timeassigned DESC')
                    ->setMaxResults(1)
                    ->setParameters(array(
                        'ndr_id' => $this->_ndr_id,
                        'car' => $i
                    ))->getSingleResult();
                $riding_result = $riding_result_raw->toArray();
            }
            catch(Exception $e)
            {
                $riding_result = array();
            }
            
			$car_riding_time = $riding_result['timeassigned'];
			
			// Looks up the last contact action.
			$contact_time = date('YmdHis', 1);
            
            try
            {
                $contact_result_raw = $this->em->createQuery('SELECT c FROM Entity\Contacted c WHERE c.ndr_id = :ndr_id AND c.carnum = :car ORDER BY c.contacttime DESC')
                    ->setMaxResults(1)
                    ->setParameters(array(
                        'ndr_id' => $this->_ndr_id,
                        'car' => $i
                    ))->getSingleResult();
                $contact_result = $contact_result_raw->toArray();
            }
            catch(Exception $e)
            {
                $contact_result = array();
            }
			
			$car_contact_time = $contact_result['contacttime'];
			$car_contact_reason = $contact_result['reason'];
			
			$car_time = max($car_riding_time, $car_done_time);
			
			$time_since_update_seconds = time() - $car_time;
			$time_since_update = round($time_since_update_seconds / 60, 0);
			
			$time_since_contact_seconds = time() - $car_contact_time;
			$time_since_contact = round($time_since_contact_seconds / 60, 0);
            
            $car = array(
                'num' => $i,
                'class' => 'car-here',
                'status' => 'Not Out',
                'couch' => ' ',
                'vehicle' => ' ',
                'drivers' => ' ',
                'notes' => ' ',
            );
			
			if ($car_pickup == "ng" && $car_time == $car_done_time && $car_time != 0)
			{
				$car['class'] = ($time_since_update > 90) ? 'car-call' : 'car-circuit';
                $car['status'] = 'On Circuit';
                $car['notes'] = 'Last dropped off '.$time_since_update.' minutes ago';
			}
			// Sets status for normal car	
			else if($car_time == $car_riding_time && $car_time != 0)
			{
				$car['class'] = ($time_since_update > 45) ? 'car-call' : 'car-normal';
                $car['status'] = 'On a Ride';
                $car['notes'] = 'Assigned '.$time_since_update.' minutes ago';
			}
			// Sets status for chilling
			else if ($car_pickup != "ng" && $car_time != 0 && $car_time == $car_done_time)
			{
				$car['class'] = ($time_since_update > 30) ? 'car-call' : 'car-chill';
                $car['status'] = 'Chilling';
                $car['notes'] = 'Last done '.$time_since_update.' minutes ago';
			}
			
			if ($time_since_contact < $time_since_update)
			{
                switch($car_contact_reason)
                {
                    case "called":
                        $car['notes'] .= '<br>Spoke '.$time_since_contact.' mins ago';
                    break;
                    
                    case "home":
                        $car['class'] = 'car-done';
                        $car['status'] = 'Home';
                        $car['notes'] = ' ';
                    break;
                    
                    case "ng":
                        $car['class'] = 'car-circuit';
                        $car['status'] = 'On Circuit';
                        $car['notes'] = 'Headed to Northgate '.$time_since_contact.' mins ago';
                    break;
                }
			}
            
            // Cars
            if (isset($ndr_positions['by_position']['car'.$i]))
            {
                $car_users_raw = $ndr_positions['by_position']['car'.$i];
                $car_users = array();
                
                foreach((array)$car_users_raw as $car_user)
                {
                    $car_users[] = $car_user['firstname'].' '.$car_user['lastname'].': '.$car_user['cphone'];
                }
                
                $car['drivers'] = '<small>'.implode("<br>", $car_users).'</small>';
            }
            
            // Vehicles
            if (isset($ndr_data['cars'][$i]))
            {
                $car_data = $ndr_data['cars'][$i];
                
                $vehicle = $car_data['color'].' '.$car_data['make'].' '.$car_data['model'].'<br><small>'.$car_data['license'].'</small>';
                $car['vehicle'] = $vehicle;
                
                $car['couch'] = $car_data['couch'];
            }
            
			$cars[$i] = $car;
		}
		
		$this->view->cars = $cars;
	}
	
	public function statusAction()
	{
		$car_num = (int)$this->_getParam('carnum');
		
		$contact = new Contacted();
        $contact->ndr = $this->_ndr;
		$contact->carnum = $car_num;
		$contact->reason = $this->_getParam('status');
		$contact->ridedate = new \DateTime('NOW');
		$contact->contacttime = new \DateTime('NOW');
		$contact->save();
		
		$this->redirectToRoute(array('module' => 'phoneroom', 'controller' => 'cars', 'action' => 'index', 'carnum' => NULL, 'status' => NULL));
		return;
	}
}