<?php
namespace Entity;


/**
 * Events
 *
 * @Table(name="events")
 * @Entity
 */
class Event extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->signups = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @Column(name="num", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $num;

    /** @Column(name="is_open", type="integer", length=1, nullable=true) */
    protected $is_open;

    /** @Column(name="title", type="string", length=50, nullable=true) */
    protected $title;

    /** @Column(name="event_date", type="date", length=25) */
    protected $event_date;

    /** @Column(name="category", type="string", length=50, nullable=true) */
    protected $category;

    /** @Column(name="team_id", type="integer", length=2, nullable=true) */
    protected $team_id;

    /** @Column(name="max_ppl", type="integer", length=4, nullable=true) */
    protected $max_ppl;

    /** @Column(name="event_time", type="string", length=255, nullable=true) */
    protected $event_time;

    /** @Column(name="location", type="text", length=255, nullable=true) */
    protected $location;

    /** @Column(name="description", type="text", nullable=true) */
    protected $description;

    /** @Column(name="dic_contact", type="text", nullable=true) */
    protected $dic_contact;

    /** @Column(name="extra_points", type="integer", nullable=true) */
    protected $extra_points;
    
    /** @OneToOne(targetEntity="Entity\Ndr", mappedBy="event") */
    protected $ndr;
    
    /** @OneToMany(targetEntity="Signup", mappedBy="event", cascade={"remove"}) */
    protected $signups;
    
    /**
     * Static Functions
     */

	public static function fetchByDateRange($date_start, $date_end)
	{
        $em = \Zend_Registry::get('em');
        return $em->createQuery('SELECT e FROM '.__CLASS__.' e WHERE e.event_date >= :start AND e.event_date <= :end ORDER BY e.event_date DESC')
            ->setParameter('start', date('Y-m-d', $date_start))
            ->setParameter('end', date('Y-m-d',  $date_end))
            ->getArrayResult();
	}
}