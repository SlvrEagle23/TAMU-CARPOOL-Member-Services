<?php
return array(
    'default' => array(
		// Files
		'files' => array(
			'label'		=> 'Files',
			'module'	=> 'files',
			'permission' => 'access member features',
			'pages'		=> array(
				'file_edit'		=> array(
					'module'		=> 'files',
					'controller'	=> 'index',
					'action'		=> 'edit',
				),
			),
		),
    ),
);
