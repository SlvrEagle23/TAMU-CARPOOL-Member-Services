<?php
namespace Entity;


/**
 * Signups
 *
 * @Table(name="signups")
 * @Entity
 */
class Signup extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="num", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $num;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;

    /** @Column(name="event_id", type="integer") */
    protected $event_id;

    /** @Column(name="timestamp", type="integer", length=4) */
    protected $timestamp;

    /** @Column(name="status", type="integer", length=1) */
    protected $status;

    /**
     * @ManyToOne(targetEntity="Event", inversedBy="signups")
     * @JoinColumn(name="event_id", referencedColumnName="num", onDelete="CASCADE")
     */
    protected $event;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="signups")
     * @JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;

}