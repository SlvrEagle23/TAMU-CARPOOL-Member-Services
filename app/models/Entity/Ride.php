<?php
namespace Entity;


/**
 * Rides
 *
 * @Table(name="rides")
 * @Entity
 */
class Ride extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="num", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $num;

    /** @Column(name="ndr_id", type="integer") */
    protected $ndr_id;

    /** @Column(name="name", type="string", length=50, nullable=true) */
    protected $name;

    /** @Column(name="cell", type="string", length=15, nullable=true) */
    protected $cell;

    /** @Column(name="riders", type="integer", length=4, nullable=true) */
    protected $riders;

    /** @Column(name="car", type="integer", length=4, nullable=true) */
    protected $car;

    /** @Column(name="pickup", type="string", length=200, nullable=true) */
    protected $pickup;

    /** @Column(name="dropoff", type="string", length=200, nullable=true) */
    protected $dropoff;

    /** @Column(name="notes", type="string", length=255, nullable=true) */
    protected $notes;

    /** @Column(name="clothes", type="string", length=200, nullable=true) */
    protected $clothes;

    /** @Column(name="ridedate", type="datetime", length=25, nullable=true) */
    protected $ridedate;

    /** @Column(name="status", type="string", length=50, nullable=true) */
    protected $status;

    /** @Column(name="timetaken", type="datetime", length=25, nullable=true) */
    protected $timetaken;

    /** @Column(name="timeassigned", type="datetime", length=25, nullable=true) */
    protected $timeassigned;

    /** @Column(name="timedone", type="datetime", length=25, nullable=true) */
    protected $timedone;

    /** @Column(name="location", type="string", length=100, nullable=true) */
    protected $location;

    /** @Column(name="is_confirmed", type="integer", length=1, nullable=true) */
    protected $is_confirmed = 0;

    /**
     * @ManyToOne(targetEntity="Ndr", inversedBy="rides")
     * @JoinColumn(name="ndr_id", referencedColumnName="ndr_id", onDelete="CASCADE")
     */
    protected $ndr;
}