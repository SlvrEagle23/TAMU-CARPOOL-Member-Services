<?php
namespace Entity;


/**
 * Teams
 *
 * @Table(name="teams")
 * @Entity
 */
class Team extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->members = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @var integer $team_id
     *
     * @Column(name="team_id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $team_id;

    /** @Column(name="team_name", type="string", length=200) */
    protected $team_name;

    /** @OneToMany(targetEntity="User", mappedBy="team") */
    protected $members;

}