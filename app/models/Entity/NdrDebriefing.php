<?php
namespace Entity;

/**
 * @Table(name="ndr_debriefings")
 * @Entity
 */
class NdrDebriefing extends \DF\Doctrine\Entity
{   
    public function __construct()
    {
        $this->is_high_priority = false;
    }
    
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="ndr_id", type="integer") */
    protected $ndr_id;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;

    /** @Column(name="debriefing_data", type="json") */
    protected $debriefing_data;

    /** @Column(name="is_high_priority", type="boolean", nullable=true) */
    protected $is_high_priority;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="ndr_debriefings")
     * @JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ManyToOne(targetEntity="Ndr", inversedBy="debriefings")
     * @JoinColumn(name="ndr_id", referencedColumnName="ndr_id", onDelete="CASCADE")
     */
    protected $ndr;
    
}